'''
Arnaud Allemand, Abdellah Oumil et Pierre Coralie
Projet Prep'ISIMA 2 - 2020
Billard planéraire (Tuteur Jean-Florent Raymond)

Dans cette simulation les corps sont représentés par des balles dont la taille
est proportionnelle à leur masse. Elles sont soumises à des forces d'attraction,
de frottement, de rebonds.

Fonctionnalités :
- clique pour ajouter des corps
- appuie sur espace pour mettre en pause
'''


import pygame
import time
import Vector2D as V2D
import Body as b
import random as rd


pygame.init()


WIDTH = 640
HEIGHT = 480
black = (0, 0, 0, 20)
pause = False

G = 0.8 #Valeur arbitraire
#G = 6.67430E-11

def applyForces(liste_de_corps, j, i):
    '''
    Calculs des différentes forces, des nouvelles accelerations et positions
    sur tous les corps
    '''
    attractionForce = liste_de_corps[j].attraction(liste_de_corps[i], G)
    frottement = liste_de_corps[i].forceFrot(1.292)
    forces = frottement.add(attractionForce)
    liste_de_corps[i].calculAcceleration(forces)
    liste_de_corps[i].calculNouvellePosition()
    liste_de_corps[i].edges(WIDTH, HEIGHT, 0.5) #0.5 coefficient de resitution sur du bois


# initialisation de la liste des corps avec 2 corps au hasard
liste_de_corps = []
for i in range(2):
    pos = V2D.Vector2D(rd.randint(1, WIDTH), rd.randint(1, HEIGHT))
    color = (rd.randint(1, 255), rd.randint(1, 255), rd.randint(1, 255))
    liste_de_corps.append(b.Body(pos, rd.randint(1,20), color))

# initialisation de la fenêtre
resolution = (WIDTH, HEIGHT)
fenetre_surface = pygame.display.set_mode(resolution)
rect_border = pygame.Surface(resolution)

clock = pygame.time.Clock()

continu = True

#path
rect = pygame.Surface((WIDTH,HEIGHT))
rect.set_alpha(32)
rect.fill((0,0,0))

while continu :

    #fenetre_surface.fill(black)
    fenetre_surface.blit(rect, (0,0))

    #gestion des evenements
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            continu = False
        if event.type == pygame.MOUSEBUTTONDOWN:
            posx = pygame.mouse.get_pos()[0]
            posy = pygame.mouse.get_pos()[1]
            color = (rd.randint(1, 255), rd.randint(1, 255), rd.randint(1, 255))
            newBody = b.Body(V2D.Vector2D(posx, posy), rd.randint(1,20), color)
            liste_de_corps.append(newBody)
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                pause = not pause

    if not pause :
        #calculs
        for i in range(len(liste_de_corps)):
            for j in range(len(liste_de_corps)):
                if i != j:
                    applyForces(liste_de_corps, j, i)
        for i in range(len(liste_de_corps)):
            for j in range(len(liste_de_corps)):
                if i != j:
                    liste_de_corps[i].bounces(liste_de_corps[j])
            liste_de_corps[i].setLocation()
            liste_de_corps[i].display(fenetre_surface)


        pygame.display.update()
        clock.tick(30)

pygame.quit()
