import math
import pygame
import Vector2D as V2D
import numpy as np
import sympy as sm

sm.var('x y')

class Body:
    '''
    Cette classe permet de déclarer un corps et de le gérer en permettant de
    faire  les différents calculs de forces, d'acceleration ainsi que de position
    '''

    def __init__(self, pos, masse, color, rayon = 1):
        self.masse = masse
        self.location = V2D.Vector2D(pos.x, pos.y)
        self.velocity = V2D.Vector2D(0, 0)
        self.acceleration = V2D.Vector2D(0, 0)
        self.color = color
        self.rayon = self.constrain(int((rayon*self.masse)/2), 5, 30)

    @staticmethod
    def constrain(val, min_val, max_val):
        '''
        Pour limiter la valeur de la distance si distance en pixel trop petite
        -> force trop grande et vice versa
        '''
        return min(max_val, max(min_val, val))

    def attraction(self, other, G):
        '''
        Calcul de la force d'attraction entre 2 corps
        '''
        #Calcul de direction
        attractionForce = self.location.sub(other.location)
        distance = attractionForce.magnitude()
        #print("distance", distance)
        distance = self.constrain(distance, 5, 10)
        attractionForce = attractionForce.normalize()
        #print("direction", attractionForce)
        #norme
        norme = (G * other.masse * self.masse) / (distance**2)
        #Calcul de la force gravitationnelle F = direction * norme
        attractionForce = attractionForce.mul(norme)
        #print("attraction force = ", attractionForce)
        return attractionForce


    def forceFrot(self, rho):
        '''
        Force de frottement sur un solide dans un liquide ou un gaz :
        F=1/2*C*S*rho*v^2
        C est un coefficient dépendant de la forme du corps (0,5 environ pour une sphère)
        S est la section apparente où il y a des frottements
        rho est la masse volumique du fluide (en paramètre)
        v est la vitesse du corps
        '''
        frottement = V2D.Vector2D(0,0)
        surface = 4 * np.pi * self.rayon**2
        frottement = frottement.mul(1/5 * 0.5 * surface * rho)
        return frottement


    def conservationQuantiteMouvement(self, other):
        '''
            Calcul les nouvelles vitesses après collision de deux corps
        '''
        #somme masses des deux corps
        M = self.masse + other.masse
        #vs_vo <=> soustraction des vecteur vitesse vitesse_self - vitesse_other
        vs_vo = self.velocity.sub(other.velocity)
        #ps_po <=> soustraction des vecteur position position_self - position_other
        ps_po = self.location.sub(other.location)
        #vo_vs <=> soustraction des vecteur vitesse vitesse_other - vitesse_self
        vo_vs = other.velocity.sub(self.velocity)
        #po_ps <=> soustraction des vecteur position_other - position_self
        po_ps = other.location.sub(self.location)

        #produit_scalaire_o <=> celui utilisé pour calculer la nouvelle vitesse de other
        produit_scalaire_o = vo_vs.scalarProduct(po_ps)
        #produit_scalaire_s <=> celui utilisé pour calculer la nouvelle vitesse de self
        produit_scalaire_s = vs_vo.scalarProduct(ps_po)

        #norme au carré de ps_po
        norm_s = ((self.location.sub(other.location)).magnitude())**2
        #norme au carré de po_ps
        norm_o = ((other.location.sub(self.location)).magnitude())**2

        #Calcul intermmediaire
        #pour self
        tmp_s = ps_po.mul((2*other.masse/M)*(produit_scalaire_s/norm_s))
        #pour other
        tmp_o = po_ps.mul((2*self.masse/M)*(produit_scalaire_o/norm_o))
        #print("CORPS 1: Vitesse Initiale: ", self.velocity)
        #nouvelle_vitesse self = vitesse_initiale * tmp_s
        self.velocity = self.velocity.sub(tmp_s)
        self.velocity.mul(-1)
        #print("CORPS 1: Vitesse Après choc: ", self.velocity)
        #print("CORPS 2: Vitesse Initiale: ", other.velocity)
        #nouvelle_vitesse other = vitesse_initiale * tmp_o
        other.velocity = other.velocity.sub(tmp_o)
        other.velocity = other.velocity.mul(-1)
        #print("CORPS 2: Vitesse Après choc: ", other.velocity)


    def isCollision(self, other):
        '''
        Retourne vrai si 2 corps entrent en contact
        faux sinon
        '''
        dist_corpse = np.sqrt((self.location.x - other.location.x)**2 + (self.location.y - other.location.y)**2)
        dist_collision = self.rayon + other.rayon
        if dist_corpse <= dist_collision:
            return True
        else:
            return False


    def bounces(self, other):
        if self.isCollision(other):
            self.conservationQuantiteMouvement(other)


    def edges(self, w, h, e):
        '''
        Gestion des bords de la fenêtre :
        on fait rebondir les corps
        '''
        # bord droit
        if (self.location.x > w):
            self.location.x = w
            self.velocity.x = (self.velocity.x * e) * -1
        # bord gauche
        elif (self.location.x < 0):
            self.location.x = 0
            self.velocity.x = (self.velocity.x * e) * -1
        # bord bas
        if (self.location.y > h):
            self.location.y = h
            self.velocity.y = (self.velocity.y * e) * -1
        # bord haut
        elif(self.location.y < 0):
            self.location.y = 0
            self.velocity.y = (self.velocity.y * e) * -1


    def calculAcceleration(self, forces):
        '''
        Calcul de l'acceleration d'un corps après le calcul des forces qui
        s'exercent sur lui
        '''
        maforce = forces.div(self.masse)
        self.acceleration = self.acceleration.add(maforce)


    def calculNouvellePosition(self):
        '''
        Calcul de la nouvelle position d'un corps après le calcul de son
        acceleration
        '''
        self.velocity = self.velocity.add(self.acceleration)
        new_location = self.location.add(self.velocity)
        self.acceleration = self.acceleration.mul(0) #remise a zero de l'acceleration pour eviter l'accumulation des valeurs
        return new_location


    def setLocation(self):
        self.location = self.calculNouvellePosition()


    def setVelocity(self, velocity):
        self.velocity = velocity


    def display(self, surface):
        pygame.draw.circle(surface, self.color, [int(self.location.x), int(self.location.y)], self.rayon)
