import math

class Vector2D:
    '''
    Cette classe permet de gérer simplement les données sur 2 axes
    source : Beginning Game Development with Python and Pygame: From Novice to Professional
    De Will McGugan
    '''

    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y

    def __str__(self):
        return "(%s %s)"%(self.x, self.y)

    @staticmethod
    def from_points(P1, P2):
        return Vector2D(P2[0] - P1[0], P2[1] - P1[1])

    def magnitude(self):
        return math.sqrt(self.x**2 + self.y**2)

    def normalize(self):
        mag = self.magnitude()
        self.x = self.x / mag
        self.y = self.y / mag
        return Vector2D(self.x, self.y)

    def add(self, other):
        return Vector2D(self.x + other.x, self.y + other.y)

    def sub(self, other):
        return Vector2D(self.x - other.x, self.y - other.y)

    def neg(self):
        return Vector2D(-self.x, -self.y)

    def mul(self, scalaire):
        return Vector2D(self.x*scalaire, self.y*scalaire)

    def div (self, scalaire):
        return Vector2D(self.x / scalaire, self.y / scalaire)

    def scalarProduct(self, other):
        return (self.x * other.x) + (self.y * other.y)
